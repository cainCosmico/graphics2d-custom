/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import com.jfoenix.controls.JFXColorPicker;
import main.graphics.Linea;
import main.graphics.Pixel;
import java.awt.Point;
import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import main.graphics.Cuadro;
import main.graphics.Poligono;
import main.graphics.Rectangulo;
import main.graphics.Triangulo;

/**
 *
 * @author camilo
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML private AnchorPane contentPane;
    @FXML private JFXColorPicker colorPicker;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.colorPicker.setValue(Color.BLACK);
//        Linea lh = new Linea(
//                new Point(20, 50), 
//                new Point(200, 50), 
//                getColor());
//        
//        Linea lv = new Linea(
//                new Point(100, 50), 
//                new Point(100, 200), 
//                getColor());
//        
//        contentPane.getChildren().addAll(lh.getPointsInLine());
//        contentPane.getChildren().addAll(lv.getPointsInLine());
    }       
    
    private void finishHandler() {
        contentPane.setOnMouseClicked(null);
        contentPane.setOnMousePressed(null);
        contentPane.setOnMouseReleased(null);
    }
    
    @FXML void onPunto() {
        this.contentPane.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                System.out.println("Event - X:" + event.getX() + " Y: " + event.getY());
                Pixel p = new Pixel(new Point(
                        (int)event.getX(), 
                        (int)event.getY()),
                        getColor());
                contentPane.getChildren().add(p);
            }
        } );
    }
    
    // LINEA
    @FXML void onLinea() {
        finishHandler();
        
        Point p1 = new Point();
        Point p2 = new Point();
        
        // Evento primer click
        this.contentPane.setOnMousePressed(new EventHandler <MouseEvent>() {
            public void handle(MouseEvent event) {
                System.out.println("Event - X:" + event.getX() + " Y: " + event.getY());
                
                int x = (int)event.getX();
                int y = (int)event.getY();                                
                p1.setLocation(x, y);
                
                event.setDragDetect(true);
            }
        });

        // Evento soltar click
        this.contentPane.setOnMouseReleased(new EventHandler <MouseEvent>()  {
            public void handle(MouseEvent event)  {
                System.out.println("Event - X:" + event.getX() + " Y: " + event.getY());
                
                int x = (int)event.getX();
                int y = (int)event.getY();                                
                p2.setLocation(x, y);
                
                contentPane.setMouseTransparent(false);
                drawLinea(p1,p2);
            }
        });               
    }
    
    public void drawLinea(Point p1,Point p2) {
        Linea linea = new Linea(p1,p2,this.getColor());        
        this.contentPane.getChildren().addAll(linea.getPointsInLine());
    }
    
    // CUADRO
    @FXML void onCuadro() {
        finishHandler();
        
        Point p1 = new Point();
        Point p2 = new Point();
        
        // Evento primer click
        this.contentPane.setOnMousePressed(new EventHandler <MouseEvent>() {
            public void handle(MouseEvent event) {
                System.out.println("Event - X:" + event.getX() + " Y: " + event.getY());
                
                int x = (int)event.getX();
                int y = (int)event.getY();                                
                p1.setLocation(x, y);
                
                event.setDragDetect(true);
            }
        });

        // Evento soltar click
        this.contentPane.setOnMouseReleased(new EventHandler <MouseEvent>()  {
            public void handle(MouseEvent event)  {
                System.out.println("Event - X:" + event.getX() + " Y: " + event.getY());
                
                int x = (int)event.getX();
                int y = (int)event.getY();                                
                p2.setLocation(x, y);
                
                contentPane.setMouseTransparent(false);
                drawCuadro(p1,p2);
            }
        });               
    }
    
    public void drawCuadro(Point p1,Point p2) {
        Cuadro cuadro = new Cuadro(p1,p2,this.getColor());        
        this.contentPane.getChildren().addAll(cuadro.getPoints());
    }
    
    // RECTANGULO
    @FXML void onRectangulo() {
        finishHandler();
        
        Point p1 = new Point();
        Point p2 = new Point();
        
        // Evento primer click
        this.contentPane.setOnMousePressed(new EventHandler <MouseEvent>() {
            public void handle(MouseEvent event) {
                System.out.println("Event - X:" + event.getX() + " Y: " + event.getY());
                
                int x = (int)event.getX();
                int y = (int)event.getY();                                
                p1.setLocation(x, y);
                
                event.setDragDetect(true);
            }
        });

        // Evento soltar click
        this.contentPane.setOnMouseReleased(new EventHandler <MouseEvent>()  {
            public void handle(MouseEvent event)  {
                System.out.println("Event - X:" + event.getX() + " Y: " + event.getY());
                
                int x = (int)event.getX();
                int y = (int)event.getY();                                
                p2.setLocation(x, y);
                
                contentPane.setMouseTransparent(false);
                drawRectangulo(p1,p2);
            }
        });               
    }
    
    public void drawRectangulo(Point p1,Point p2) {
        Rectangulo rectangulo = new Rectangulo(p1,p2,this.getColor());        
        this.contentPane.getChildren().addAll(rectangulo.getPoints());
    }
    
    // TRIANGULO
    @FXML void onTriangulo() {
        finishHandler();
        
        Point p1 = new Point();
        Point p2 = new Point();
        
        // Evento primer click
        this.contentPane.setOnMousePressed(new EventHandler <MouseEvent>() {
            public void handle(MouseEvent event) {
                System.out.println("Event - X:" + event.getX() + " Y: " + event.getY());
                
                int x = (int)event.getX();
                int y = (int)event.getY();                                
                p1.setLocation(x, y);
                
                event.setDragDetect(true);
            }
        });

        // Evento soltar click
        this.contentPane.setOnMouseReleased(new EventHandler <MouseEvent>()  {
            public void handle(MouseEvent event)  {
                System.out.println("Event - X:" + event.getX() + " Y: " + event.getY());
                
                int x = (int)event.getX();
                int y = (int)event.getY();                                
                p2.setLocation(x, y);
                
                contentPane.setMouseTransparent(false);
                drawTriangulo(p1,p2);
            }
        });               
    }
    
    public void drawTriangulo(Point p1,Point p2) {
        Triangulo triangulo = new Triangulo(p1,p2,this.getColor());        
        this.contentPane.getChildren().addAll(triangulo.getPoints());
    }
    
    // POLIGONO
    @FXML void onPoligono() {
        finishHandler();
        
        LinkedList<Point> coor = new LinkedList<>();
        
        // Evento primer click
        this.contentPane.setOnMouseClicked(new EventHandler <MouseEvent>() {
            public void handle(MouseEvent event) {
                System.out.println("Event - X:" + event.getX() + " Y: " + event.getY());
                
                int x = (int)event.getX();
                int y = (int)event.getY();                                
                Point aux = new Point(x, y);
                coor.add(aux);
                if(event.getClickCount() == 2){
                    System.out.println("Double clicked");
                    drawPoligono(coor);
                    coor.clear();
                }     
                
            }
        });            
    }
    
    public void drawPoligono(LinkedList<Point> points) {
        Poligono poligono = new Poligono(points,this.getColor());        
        this.contentPane.getChildren().addAll(poligono.getPoints());
    }
    
    public Color getColor() {
        return this.colorPicker.getValue();
    }   
          
}
