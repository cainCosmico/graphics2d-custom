/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.graphics;

import java.awt.Point;
import java.util.LinkedList;
import javafx.scene.paint.Color;

/**
 *
 * @author camilo
 */
public class Rectangulo {
    private final Point p1;
    private final Point p2;
    private final Color color;
    private final LinkedList<Pixel> points;
    
    public Rectangulo(Point p1, Point p2, Color color) {
        this.p1 = p1;
        this.p2 = p2;
        this.color = color;
        this.points = new LinkedList<>();
        if (p1.getX() > p2.getX()) this.swap(p1, p2);
        draw();
    }
    
    public void draw() {
        Linea l1 = new Linea(
                p1, 
                new Point((int)p2.getX(), (int)p1.getY()), 
                color);
        Linea l2 = new Linea(
                new Point((int)p2.getX(), (int)p1.getY()),
                p2, 
                color);
        Linea l3 = new Linea(
                new Point((int)p1.getX(), (int)p2.getY()),
                p2, 
                color);
        Linea l4 = new Linea(
                p1,
                new Point((int)p1.getX(), (int)p2.getY()), 
                color);
        
        this.points.addAll(l1.getPointsInLine());
        this.points.addAll(l2.getPointsInLine());
        this.points.addAll(l3.getPointsInLine());
        this.points.addAll(l4.getPointsInLine());
    }
    
    public LinkedList<Pixel> getPoints() {
        return this.points;
    }    
    
    public void swap(Point p1, Point p2) {
        Point aux;
        aux = p1;
        p1 = p2; 
        p2 = aux;
    }   
}
