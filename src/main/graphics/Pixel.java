/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.graphics;

import java.awt.Point;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

/**
 *
 * @author camilo
 */
public class Pixel extends Line {
    Point point;
    Color color;
    
    public Pixel(Point point, Color color) {
        super(point.getX(),point.getY(),point.getX(),point.getY());
        this.setStroke(color);
    }
    
    public Pixel getPixel() {
        return this;
    }
}
