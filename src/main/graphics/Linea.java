/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.graphics;

import java.awt.Point;
import java.util.LinkedList;
import javafx.scene.paint.Color;

/**
 *
 * @author camilo
 */
public class Linea {
    private final Point p1;
    private final Point p2;
    private final Color color;
    private final LinkedList<Pixel> points;
    
    public Linea(Point p1, Point p2, Color color) {
        this.p1 = p1;
        this.p2 = p2;
        this.color = color;
        points = new LinkedList<>();
        draw();
    }
    
    public void draw() {
        if (this.p1.getX() > this.p2.getX()) this.swap(this.p1, this.p2);
        
        if (this.p1.getY() == this.p2.getY()) drawHorizontal();
        else if (this.p1.getX() == this.p2.getX()) drawVertical();
        else {
//            // dibuja con respecto a X
            for (int i=(int)p1.getX(); i<=(int)p2.getX(); i++) {
                double fy = getM()*(i-p1.getX())+p1.getY();
    //            System.out.println("fy: " + fy + " int: " + (int)fy);
                this.points.add(new Pixel(
                        new Point(i,(int)fy), this.color));
            }
            
            // dibuja con respecto a Y
            for (int i=(int)p1.getY(); i<=(int)p2.getY(); i++) {
                double fx = ( (i-p1.getY()) + getM()*p1.getX() ) / getM();
    //            System.out.println("fx: " + fx + " int: " + (int)fx);
                this.points.add(new Pixel(
                        new Point((int)fx,i), this.color));
            }
        }
    }
    
    public void drawHorizontal() {
        System.out.println("Linea Horizontal");
        for (int i=(int)p1.getX(); i<=(int)p2.getX(); i++) {
            this.points.add(new Pixel(
                    new Point(i,(int)p1.getY()), this.color));
        }
    }
    
    public void drawVertical() {
        System.out.println("Linea Vertical");
        for (int i=(int)p1.getY(); i<=(int)p2.getY(); i++) {
            this.points.add(new Pixel(
                    new Point((int)p1.getX(),i), this.color));
        }
    }
    
    public void swap(Point p1, Point p2) {
        System.out.println("Swap");
        System.out.println("p1: " + p1.toString() + " p2: " + p2.toString());
        Point aux;
        aux = p1;
        p1 = p2; 
        p2 = aux;
        System.out.println("p1: " + p1.toString() + " p2: " + p2.toString());
    }    
    
    /**
     * Calcula la pendiente de los puntos del constructor
     * @return 
     */
    public double getM() {
        return (p2.getY() - p1.getY())/(p2.getX() - p1.getX());
    }
    
    public LinkedList<Pixel> getPointsInLine() {
        return this.points;
    }
    
//    public static void main(String[] args) {
//        Point p1 = new Point(2, 3);
//        Point p2 = new Point(4, 5);
//        Linea myPixel = new Linea(p1, p2, Color.BLUE);
//        myPixel.draw();
//    }
}
