/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.graphics;

import java.awt.Point;
import java.util.LinkedList;
import javafx.scene.paint.Color;

/**
 *
 * @author camilo
 */
public class Poligono {
    private LinkedList<Point> coordinates;
    private LinkedList<Pixel> points;
    private Color color;
    
    public Poligono(LinkedList<Point> coordinates, Color color) {
        this.coordinates = coordinates;
        this.color = color;
        this.points = new LinkedList<>();
        draw();
    }
    
    public void draw() {
        if (this.coordinates.size()<2) {
            System.out.println("Insuficientes puntos para dibujar poligono");
            return;
        }
        
        Point puntoInicial = coordinates.get(0);
        Point puntoReferencia = coordinates.get(0);
        Linea aux;
        for (int i=1; i<coordinates.size(); i++) {
            aux = new Linea(puntoReferencia, coordinates.get(i), color);
            puntoReferencia =  coordinates.get(i);
            points.addAll(aux.getPointsInLine());
            aux = null;
        }
        aux = new Linea(puntoInicial,puntoReferencia, color);
        this.points.addAll(aux.getPointsInLine());
    }
    
    public LinkedList<Pixel> getPoints() {
        return this.points;
    }
}
